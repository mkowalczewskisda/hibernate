package com.sda.dao;

import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.model.User;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class OrderDao extends GenericDao<Order> {

  public OrderDao() {
    super(Order.class);
  }

  public List<Order> findByDate(LocalDateTime dateTime) {
    Session session = openSession();
    Query<Order> query = session.createQuery("Select o from Order o where o.orderDate = :date")
        .setParameter("date", dateTime);
    List<Order> orders = query.getResultList();
    session.close();;
    return orders;
  }

  public List<Order> findByPriceLowerThan(double price) {
    Session session = openSession();
    Query<Order> query = session.createQuery("Select o from Order o where o.orderPrice < :price")
        .setParameter("price", BigDecimal.valueOf(price));
    List<Order> orders = query.getResultList();
    session.close();
    return orders;
  }

  public List<Order> findBeforeDate(LocalDateTime dateTime) {
    Session session = openSession();
    Query<Order> query = session.createQuery("Select o from Order o where o.orderDate < :date order by o.orderDate")
        .setParameter("date", dateTime);
    List<Order> orders = query.getResultList();
    session.close();
    return orders;
  }

  public List<Order> findAfterDate(LocalDateTime dateTime) {
    Session session = openSession();
    Query<Order> query = session.createNativeQuery("Select * from `Order` where ORD_DATE > :date order by ORD_DATE desc"
        , Order.class)
        .setParameter("date", dateTime);
    List<Order> orders = query.getResultList();
    session.close();
    return orders;
  }

  public List<Order> findByUser(User user) {
    Session session = openSession();
    Query<Order> query = session.createQuery("Select o from Order o join fetch o.products p where o.user = :user")
        .setParameter("user", user);
    List<Order> orders = query.getResultList();
    session.close();;
    return orders;
  }

  public List<Order> findByProduct(Product product) {
    Session session = openSession();
    Query<Order> query = session.createQuery("Select o from Order o join fetch o.products p where p = :product")
        .setParameter("product", product);
    List<Order> orders = query.getResultList();
    session.close();;
    return orders;
  }

}
