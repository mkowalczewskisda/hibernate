package com.sda.dao;

import com.sda.model.Address;
import com.sda.model.User;
import com.sda.model.User_;
import com.sda.util.HibernateUtil;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.hibernate.Session;

public class UserCriteriaDao {

  Session session;
  CriteriaBuilder criteriaBuilder;
  CriteriaQuery<User> criteriaQuery;
  Root<User> root;

  void configureFrom() {
    session = HibernateUtil.getSessionFactory().openSession();
    criteriaBuilder = session.getCriteriaBuilder();
    criteriaQuery = criteriaBuilder.createQuery(User.class);
    root = criteriaQuery.from(User.class);
  }

  public List<User> findUserWhereNameContains(String s) {
    configureFrom();
    root.fetch(User_.address);
    criteriaQuery.select(root).where(criteriaBuilder
        .like(root.get(User_.lastName),
        "%"+s+"%"));
    Query query = session.createQuery(criteriaQuery);
    List<User> users = query.getResultList();
    session.close();
    return users;
  }

}
