package com.sda;

import com.sda.dao.GenericDao;
import com.sda.dao.OrderDao;
import com.sda.dao.UserCriteriaDao;
import com.sda.model.Order;
import com.sda.model.Product;
import com.sda.model.User;
import java.time.LocalDateTime;
import java.util.List;

public class zadanieOrderDao2 {

  public static void main(String[] args) {
    OrderDao orderDao = new OrderDao();
    //GenericDao<Order> genericDao = new GenericDao<>(Order.class);
    //System.out.println(genericDao.findById(2));
    System.out.println(orderDao.findById(2));

    List<Order> orders = orderDao.findByDate(LocalDateTime.parse("2020-03-30T13:42:48"));
    orders.forEach(System.out::println);

    orderDao.findByPriceLowerThan(30.0).forEach(System.out::println);
    orderDao.findBeforeDate(LocalDateTime.parse("2020-08-26T13:42:48"))
        .forEach(System.out::println);
    orderDao.findAfterDate(LocalDateTime.parse("2020-05-24T13:42:48"))
        .forEach(System.out::println);

    GenericDao<User> userDao = new GenericDao<>(User.class);
    User user = userDao.findById(1);
    orderDao.findByUser(user).forEach(System.out::println);

    GenericDao<Product> productDao = new GenericDao<>(Product.class);
    orderDao.findByProduct(productDao.findById(1))
        .forEach(order -> order.getProducts().forEach(System.out::println));

    UserCriteriaDao userCriteriaDao = new UserCriteriaDao();
    List<User> users = userCriteriaDao.findUserWhereNameContains("B");
    users.forEach(System.out::println);
    users.forEach(user1 -> System.out.println(user1.getAddress()));

  }

}
